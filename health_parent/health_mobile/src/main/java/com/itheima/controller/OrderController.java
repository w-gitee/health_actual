package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.aliyuncs.exceptions.ClientException;
import com.itheima.constant.MessageConstant;
import com.itheima.constant.RedisMessageConstant;
import com.itheima.entity.Result;
import com.itheima.pojo.Order;
import com.itheima.service.OrderService;
import com.itheima.utils.SMSUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisPool;

import java.util.Map;

@RestController
@RequestMapping("/order")
public class OrderController {
    
    @Autowired
    private JedisPool jedisPool;
    
    @Reference
    private OrderService orderService;
    
    
    @RequestMapping("/submit")
    public Result submit(@RequestBody Map map) {
        String telephone = (String) map.get("telephone");
        String validateCode = (String) map.get("validateCode");
        /*从redis数据库中获取验证码*/
        String redisValidateCode = jedisPool.getResource().get(telephone + RedisMessageConstant.SENDTYPE_ORDER);
        
       /*暂不校验验证码，方便测试
       if (redisValidateCode == null || !validateCode.equals(redisValidateCode)) {
            return new Result(false, MessageConstant.ORDERSETTING_FAIL);
        }
        */
        /*注意设置预约方式*/
        Result result = null;
        try {
            map.put("orderType", Order.ORDERTYPE_WEIXIN);
            result = orderService.submit(map);
        } catch (Exception e) {
            e.printStackTrace();
            return result;
        }
        if(result.isFlag()){
            String orderData = (String) map.get("orderData");
            try {
                SMSUtils.sendShortMessage(MessageConstant.ORDER_SUCCESS,telephone,orderData);
            } catch (ClientException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    
    @RequestMapping("/findById")
    public Result findById(Integer id){
        try {
            Map map = orderService.findById(id);
            return new Result(true,MessageConstant.QUERY_ORDER_SUCCESS,map);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.QUERY_ORDER_FAIL);
        }
    }
}
