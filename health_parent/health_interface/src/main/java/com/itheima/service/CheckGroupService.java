package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckGroup;

import java.util.List;

public interface CheckGroupService {
    void add(CheckGroup checkGroup, Integer[] checkitemIds);
    
    PageResult pageQuery(QueryPageBean queryPageBean);
    
    CheckGroup findById(Integer id);
    
    List<Integer> findCheckItemsByCheckGroupId(Integer id);
    
    void edit(CheckGroup checkGroup, Integer[] checkitemIds);
    
    List<CheckGroup> findAll();
    
    void delete(Integer id);
}
