package com.itheima.service;

import com.itheima.pojo.OrderSetting;

import java.util.List;
import java.util.Map;

public interface OrderSettingService {
    
    /**
     * 批量导入预约设置
     * @param list
     */
    void add(List<OrderSetting> list);
    
    List<Map> findOrderSettingByMonth(String date);
    
    void editNumberByDate(OrderSetting orderSetting);
}
