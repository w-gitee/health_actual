package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Setmeal;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

public interface SetmealService {
    void add(Setmeal setmeal, Integer[] checkgroupIds);
    
    PageResult queryPage(QueryPageBean queryPageBean);
    
    List<Setmeal> findAll();
    
    Setmeal findById(Integer id);
    
    List<Integer> findCheckGroupByCheckMealId(Integer id);
    
    void edit(Setmeal setmeal, Integer[] checkgroupIds);
    
    void delete(Integer id);
    
    List<Map<String, Object>> findSetmealNamesAndCount();
    
}
