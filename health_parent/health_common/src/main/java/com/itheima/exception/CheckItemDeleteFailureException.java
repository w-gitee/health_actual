package com.itheima.exception;

public class CheckItemDeleteFailureException extends RuntimeException{
    public CheckItemDeleteFailureException() {
    }
    
    public CheckItemDeleteFailureException(String message) {
        super(message);
    }
    
    public CheckItemDeleteFailureException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public CheckItemDeleteFailureException(Throwable cause) {
        super(cause);
    }
    
    public CheckItemDeleteFailureException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
