package com.itheima.poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class PoiTest {
    
//    @Test/* 读数据1 */
    public void poiTest1() throws IOException {
        XSSFWorkbook sheets = new XSSFWorkbook("e:\\poi.xlsx");
        XSSFSheet sheet = sheets.getSheetAt(0);
        for (Row row : sheet) {
            for (Cell cell : row) {/*遍历单元格*/
                String value = cell.getStringCellValue();
                System.out.println(value);
            }
        }
    }
    
//    @Test/* 读数据2 */
    public void poiTest2() throws IOException {
        XSSFWorkbook sheets = new XSSFWorkbook("e:\\poi.xlsx");
        XSSFSheet sheet = sheets.getSheetAt(0);
        /*行号从0开始*/
        int lastRowNum = sheet.getLastRowNum();
        for (int i = 0; i <= lastRowNum; i++) {
            XSSFRow row = sheet.getRow(i);
            /*列号从1开始*/
            short lastCellNum = row.getLastCellNum();
            for (int j = 0; j < lastCellNum; j++) {
                XSSFCell cell = row.getCell(j);
                System.out.println(cell.getStringCellValue());
            }
        }
    }
    
//    @Test/* 写数据 */
    public void poiTest3() throws IOException {
//        内存中创建工作簿
        XSSFWorkbook excel = new XSSFWorkbook();
        XSSFSheet sheet = excel.createSheet("传智播客");
        XSSFRow title = sheet.createRow(0);
        title.createCell(0).setCellValue("姓名");
        title.createCell(1).setCellValue("年龄");
        title.createCell(2).setCellValue("地址");
    
        XSSFRow dataRow = sheet.createRow(1);
        dataRow.createCell(0).setCellValue("小明");
        dataRow.createCell(1).setCellValue("20");
        dataRow.createCell(2).setCellValue("北京");
    
        //创建输出流，写入内存
        FileOutputStream fos = new FileOutputStream(new File("e:\\hello.xlsx"));
        excel.write(fos);
        fos.flush();
        excel.close();
        
    }
}
