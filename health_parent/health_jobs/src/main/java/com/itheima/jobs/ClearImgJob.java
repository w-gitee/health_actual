package com.itheima.jobs;

import com.itheima.constant.RedisConstant;
import com.itheima.utils.QiniuUtils;
import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.JedisPool;

import java.util.Set;

public class ClearImgJob {
    
    @Autowired
    private JedisPool jedisPool;
    
    public void clearImg() {
        Set<String> set = jedisPool.getResource().sdiff(RedisConstant.SETMEAL_PIC_RESOURCES, RedisConstant.SETMEAL_PIC_DB_RESOURCES);
        Set<String> set1 = jedisPool.getResource().sinter(RedisConstant.SETMEAL_PIC_RESOURCES, RedisConstant.SETMEAL_PIC_DB_RESOURCES);
        if (set != null) {
            for (String fileName : set) {
                QiniuUtils.deleteFileFromQiniu(fileName);
                jedisPool.getResource().srem(RedisConstant.SETMEAL_PIC_RESOURCES,fileName);
                
                System.out.println(fileName+"已被清除");
            }
        }
    
        if (set1 != null) {
            for (String fileName : set1) {
                QiniuUtils.deleteFileFromQiniu(fileName);
                jedisPool.getResource().srem(RedisConstant.SETMEAL_PIC_RESOURCES,fileName);
                jedisPool.getResource().srem(RedisConstant.SETMEAL_PIC_DB_RESOURCES,fileName);
            
                System.out.println(fileName+"已被清除");
            }
        }
    }
}
