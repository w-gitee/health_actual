package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.OrderSettingDao;
import com.itheima.pojo.OrderSetting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;

@Service(interfaceClass = OrderSettingService.class)
@Transactional
public class OrderSettingServiceImpl implements OrderSettingService {
    @Autowired
    private OrderSettingDao orderSettingDao;
    
    @Override
    public void add(List<OrderSetting> list) {
        for (OrderSetting orderSetting : list) {
            long count = orderSettingDao.findCountByOrderDate(orderSetting.getOrderDate());
            if (count > 0) {
                orderSettingDao.editNumberByOrderData(orderSetting);
            } else {
                orderSettingDao.add(orderSetting);
            }
        }
    }
  
    @Override
    public List<Map> findOrderSettingByMonth(String date) {
        String begin = date + "-1";
        String end = date + "-31";
        Map<String, String> map = new HashMap<>();
        map.put("begin", begin);
        map.put("end", end);
        List<OrderSetting> list = orderSettingDao.findOrderSettingByMonth(map);
        List<Map> listMap = new ArrayList<>();
        if (list != null && list.size() > 0) {
            for (OrderSetting orderSetting : list) {
                Map<String, Integer> mapData = new HashMap<>();
                mapData.put("date", orderSetting.getOrderDate().getDate());
                mapData.put("number", orderSetting.getNumber());
                mapData.put("reservations", orderSetting.getReservations());
                listMap.add(mapData);
            }
        }
        return listMap;
    }
    
    /**
     * 根据日期修改可预约数量
     */
    public void editNumberByDate(OrderSetting orderSetting) {
        Date orderDate = orderSetting.getOrderDate();
        long count = orderSettingDao.findCountByOrderDate(orderDate);
        if(count>0){
            orderSettingDao.editNumberByOrderData(orderSetting);
        }else {
            orderSettingDao.add(orderSetting);
        }
    }
    
}
