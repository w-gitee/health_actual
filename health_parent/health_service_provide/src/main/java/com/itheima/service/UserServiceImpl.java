package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.PermissionDao;
import com.itheima.dao.RoleDao;
import com.itheima.dao.UserDao;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import com.itheima.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service(interfaceClass = UserService.class)
@Transactional
public class UserServiceImpl implements UserService {
    
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private PermissionDao permissionDao;
    
    @Override
    public User findByUsername(String username) {
        User user = userDao.findByUsername(username);
        if (user == null) {
            return null;
        }
    
        Integer userId = user.getId();
        Set<Role> roleList = roleDao.findRoleByUserId(userId);
        for (Role role : roleList) {
            Integer roleId = role.getId();
            Set<Permission> permissionList = permissionDao.findPermissionByRoleId(roleId);
            role.setPermissions(permissionList);
        }
        user.setRoles(roleList);
        return user;
    }
}
