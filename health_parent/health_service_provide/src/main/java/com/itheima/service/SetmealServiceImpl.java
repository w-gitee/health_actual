package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.constant.RedisConstant;
import com.itheima.dao.SetmealDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Setmeal;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfig;
import redis.clients.jedis.JedisPool;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = SetmealService.class)
@Transactional
public class SetmealServiceImpl implements SetmealService {
    
    @Autowired
    private SetmealDao setmealDao;
    @Autowired
    private JedisPool jedisPool;
    //3、 注入FreeMarkerConfig, 以及生成的静态资源路径
    @Autowired
    private FreeMarkerConfig freeMarkerConfig;
    @Value("${out_put_path}")
    private String outPutPath;
    
    @Override
    public void add(Setmeal setmeal, Integer[] checkgroupIds) {
        setmealDao.add(setmeal);
        Integer setmealId = setmeal.getId();
        this.setAssociation(setmealId, checkgroupIds);
        
        String fileName = setmeal.getImg();
        jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_DB_RESOURCES, fileName);
        
        /*生成静态页面*/
        generateMobileStaticHtml();
        
    }
    
    /* 5、生成移动端静态页面*/
    private void generateMobileStaticHtml() {
        /*获取套餐数据，*/
        List<Setmeal> list = setmealDao.findAll();
        /**生成一个静态列表页面（所有套餐）*/
        generateMobileSetmealListHtml(list);
        //生成套餐详情静态页面（多个检查组）
        generateMobileSetmealDetailHtml(list);
    }
    
    //6、 /**生成一个静态列表页面（所有套餐）*/
    private void generateMobileSetmealListHtml(List<Setmeal> list) {
        Map map = new HashMap();
        map.put("setmealList", list);
        generateHtml("mobile_setmeal.ftl", "static_setmeal.html", map);
    }
    
    //7、 //生成套餐详情静态页面（多个检查组）
    private void generateMobileSetmealDetailHtml(List<Setmeal> list) {
        if (list != null && list.size() > 0) {
            for (Setmeal setmeal : list) {
                Map map = new HashMap();
                map.put("setmeal", setmealDao.findById(setmeal.getId()));
                generateHtml("mobile_setmeal_detail.ftl", "setmeal_detail_" + setmeal.getId() + ".html", map);
            }
        }
    }
    
    /* 4、通用生成html静态页面*/
    public void generateHtml(String templateName, String htmlPageName, Map map) {
        /*获取 freemarker的配置对象*/
        Configuration configuration = freeMarkerConfig.getConfiguration();
        Writer writer = null;
        try {
            Template template = configuration.getTemplate(templateName);
            /*构造输出流，指定文件位置，名字*/
//            writer = new FileWriter(new File(outPutPath + "/" + htmlPageName));
            writer = new OutputStreamWriter(new FileOutputStream(new File(outPutPath + "/" + htmlPageName)), "utf-8");
            template.process(map, writer);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    @Override
    public PageResult queryPage(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString = queryPageBean.getQueryString();
        
        PageHelper.startPage(currentPage, pageSize);
        Page<Setmeal> page = setmealDao.findByCondition(queryString);
        return new PageResult(page.getTotal(), page.getResult());
    }
    
    @Override
    public List<Setmeal> findAll() {
        return setmealDao.findAll();
    }
    
    @Override
    public Setmeal findById(Integer id) {
        return setmealDao.findById(id);
    }
    
    @Override
    public List<Integer> findCheckGroupByCheckMealId(Integer id) {
        return setmealDao.findCheckGroupByCheckMealId(id);
    }
    
    @Override
    public void edit(Setmeal setmeal, Integer[] checkgroupIds) {
        Setmeal setmeal4DB = setmealDao.findById(setmeal.getId());
        setmealDao.edit(setmeal);
        Integer setmealId = setmeal.getId();
        deleteAssociation(setmealId);
        setAssociation(setmealId,checkgroupIds);
        
        if (!setmeal.getImg().equals(setmeal4DB.getImg())) {
            jedisPool.getResource().srem(RedisConstant.SETMEAL_PIC_DB_RESOURCES, setmeal4DB.getImg());
            jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_DB_RESOURCES, setmeal.getImg());
        }
    }
    
    @Override
    public void delete(Integer id) {
        deleteAssociation(id);
        setmealDao.deleteById(id);
    }
    
    @Override
    public List<Map<String, Object>> findSetmealNamesAndCount() {
        return setmealDao.findSetmealNamesAndCount();
    }
    
    /**
     * 删除关联关系
     *
     * @param setmealId
     */
    private void deleteAssociation(Integer setmealId) {
        setmealDao.deleteSetmealAndCheckGroup(setmealId);
    }
    
    /**
     * 设置多对多关联关系
     */
    public void setAssociation(Integer setmealId, Integer[] checkgroupIds) {
        if (checkgroupIds != null && checkgroupIds.length > 0) {
            for (Integer checkgroupId : checkgroupIds) {
                Map<String, Integer> map = new HashMap<>();
                map.put("setmealId", setmealId);
                map.put("checkgroupId", checkgroupId);
                setmealDao.setSetmealAndCheckGroup(map);
            }
        }
    }
}
