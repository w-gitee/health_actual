package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.CheckGroupDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = CheckGroupService.class)
@Transactional
public class CheckGroupServiceImpl implements CheckGroupService {
    
    @Autowired
    private CheckGroupDao checkGroupDao;
    
    @Override
    public void add(CheckGroup checkGroup, Integer[] checkitemIds) {
        checkGroupDao.add(checkGroup);
        Integer checkGroupId = checkGroup.getId();
        this.addAssociation(checkitemIds, checkGroupId);
    }
    
    
    @Override
    public PageResult pageQuery(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString = queryPageBean.getQueryString();
        
        PageHelper.startPage(currentPage, pageSize);
        Page<CheckGroup> page = checkGroupDao.findByCondition(queryString);
        return new PageResult(page.getTotal(), page.getResult());
    }
    
    @Override
    public CheckGroup findById(Integer id) {
        return checkGroupDao.findById(id);
    }
    
    @Override
    public List<Integer> findCheckItemsByCheckGroupId(Integer id) {
        return checkGroupDao.findCheckItemsByCheckGroupId(id);
    }
    
    @Override
    public void edit(CheckGroup checkGroup, Integer[] checkitemIds) {
        checkGroupDao.edit(checkGroup); /*修改普通表单信息*/
        Integer checkGroupId = checkGroup.getId();
        checkGroupDao.deleteAssociation(checkGroupId); /*删除中间表关联关系*/
        this.addAssociation(checkitemIds, checkGroupId); /*添加中间表关联关系*/
    }
    
    @Override
    public List<CheckGroup> findAll() {
        return checkGroupDao.findAll();
    }
    
    @Override
    public void delete(Integer id) {
        Long count = checkGroupDao.findCountMealByCheckGroupId(id);
        System.out.println(count);
        if (count != null && count > 0) {
            throw new RuntimeException();
        } else {
            checkGroupDao.deleteAssociation(id);
            checkGroupDao.deleteById(id);
        }
    }
    
    /**
     * 添加检查组和检查项的管理关系(抽取出来的共公方法)
     */
    private void addAssociation(Integer[] checkitemIds, Integer checkGroupId) {
        if (checkitemIds != null && checkitemIds.length > 0) {
            for (Integer checkitemId : checkitemIds) {
                Map<String, Integer> map = new HashMap<>();
                map.put("checkGroupId", checkGroupId);
                map.put("checkitemId", checkitemId);
                checkGroupDao.setCheckGroupAndCheckItem(map);
            }
        }
    }
}
