package com.itheima.service;


import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.CheckItemDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.exception.CheckItemDeleteFailureException;
import com.itheima.pojo.CheckItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service(interfaceClass = CheckItemService.class)
@Transactional
public class CheckItemServiceImpl implements CheckItemService {
    
    @Autowired
    private CheckItemDao checkItemDao;
    
    public void add(CheckItem checkItem) {
        checkItemDao.add(checkItem);
    }
    
    public PageResult findPage(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString = queryPageBean.getQueryString();
        
        PageHelper.startPage(currentPage, pageSize);
        Page<CheckItem> page = checkItemDao.findByCondition(queryString);
        long total = page.getTotal();
        List<CheckItem> rows = page.getResult();
        return new PageResult(total, rows);
    }
    
    public void deleteById(Integer id) {
        Long count = checkItemDao.findCountByCheckItemId(id);
//        System.out.println(count);
        if (count > 0){/** 检查组里边有的检查项不能删除 */
            throw new RuntimeException();
//            throw new CheckItemDeleteFailureException();
        }
        checkItemDao.deleteById(id);
    }
    
    public CheckItem findById(Integer id) {
        return checkItemDao.findById(id);
    }
    
    public void edit(CheckItem checkItem) {
        checkItemDao.edit(checkItem);
    }
    
    public List<CheckItem> findAll() {
        return checkItemDao.findAll();
    }
}
