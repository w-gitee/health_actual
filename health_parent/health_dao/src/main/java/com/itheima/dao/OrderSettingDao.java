package com.itheima.dao;

import com.itheima.pojo.OrderSetting;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface OrderSettingDao {
    /**
     * 根据给定日期在数据库中存在的数量
     * @param orderDate
     * @return
     */
    long findCountByOrderDate(Date orderDate);
    /**
     * 根据日期修改给定日期对应的可预约人数
     * @param orderSetting
     */
    void editNumberByOrderData(OrderSetting orderSetting);
    
    /**
     * 添加预约设置
     * @param orderSetting
     */
    void add(OrderSetting orderSetting);
    
    /**
     * 根据月份查询本月所有的预约设置
     * @param map
     * @return
     */
    List<OrderSetting> findOrderSettingByMonth(Map<String, String> map);
    
    /**
     * 根据日期获取当日的预约设置，提交预约时使用，根据预约数量，已预约人数判断是否可以继续预约
     * @param parseString2Date
     * @return
     */
    OrderSetting findByOrderDate(Date parseString2Date);
    
    /**
     * 预约成功之后，更新已预约人数
     * @param orderSetting
     */
    public void editReservationsByOrderDate(OrderSetting orderSetting);
}
