package com.itheima.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.Result;
import com.itheima.pojo.OrderSetting;
import com.itheima.service.OrderSettingService;
import com.itheima.utils.POIUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 预约设置
 */
@RestController
@RequestMapping("/ordersetting")
public class OrderSettingController {
    
    @Reference
    private OrderSettingService orderSettingService;
    
    /**
     * 文件上传，上传excel文件
     * @param excelFile
     * @return
     */
    @RequestMapping("/upload")
    public Result upload(@RequestParam("excelFile") MultipartFile excelFile) {
        try {
            List<String[]> strings = POIUtils.readExcel(excelFile);
            List<OrderSetting> list = new ArrayList<>();
            if (strings != null && strings.size() > 0) {
                for (String[] string : strings) {
                    String date = string[0];
                    String number = string[1];
                    OrderSetting orderSetting = new OrderSetting(new Date(date), Integer.parseInt(number));
                    list.add(orderSetting);
                }
            }
            orderSettingService.add(list);
            return new Result(true, MessageConstant.IMPORT_ORDERSETTING_SUCCESS);
        } catch (IOException e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.IMPORT_ORDERSETTING_FAIL);
        }
    }
    
    /**
     * 根据月份查询当天预约信息
     * @param date
     * @return
     */
    @RequestMapping("/findOrderSettingByMonth")
    public Result findOrderSettingByMonth(String date) {
        try {
            List<Map> list = orderSettingService.findOrderSettingByMonth(date);
            return new Result(true,MessageConstant.GET_ORDERSETTING_SUCCESS,list);
        }catch (Exception e){
            e.getMessage();
            return new Result(false,MessageConstant.GET_ORDERSETTING_SUCCESS);
        }
    }
    
    /**
     * 根据日期修改当天可预约人数
     * @param orderSetting
     * @return
     */
    @RequestMapping("/editNumberByDate")
    public Result editNumberByDate(@RequestBody OrderSetting orderSetting) {
        try {
            orderSettingService.editNumberByDate(orderSetting);
            return new Result(true,MessageConstant.GET_ORDERSETTING_SUCCESS);
        }catch (Exception e){
            e.getMessage();
            return new Result(false,MessageConstant.GET_ORDERSETTING_SUCCESS);
        }
    }
    
}
