package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import com.itheima.pojo.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class SpringSecurityUserService implements UserDetailsService {
    /**
     * 使用dubbo远程调用服务提供方获取用户信息
     */
    @Reference
    private UserService userService;
    
    /**
     * 根据用户名查询数据库用户信息
     */
    @Override/*做认证时，框架调用该方法*/
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.findByUsername(username);/*并查出来user所关联的角色，角色关联的权限*/
        if (user == null) {
            return null;
        }
    
        List<GrantedAuthority> list = new ArrayList<>();
        
        Set<Role> roles = user.getRoles();
        for (Role role : roles) {
            list.add(new SimpleGrantedAuthority(role.getKeyword()));
            Set<Permission> permissions = role.getPermissions();
            for (Permission permission : permissions) {
                list.add(new SimpleGrantedAuthority(permission.getKeyword()));
            }
        }
    
        org.springframework.security.core.userdetails.User securityUser = new org.springframework.security.core.userdetails.User(
            username,user.getPassword(),list
        );
        return securityUser;
    }
}
