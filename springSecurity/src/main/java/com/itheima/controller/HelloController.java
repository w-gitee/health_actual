package com.itheima.controller;


import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloController {
    
    
    @RequestMapping("/add")
    @PreAuthorize("hasAnyAuthority('add')")/*要想调用此方法必须有add权限*/
    public String add(){
        System.out.println("add.");
        return "success";
    }
    
    @RequestMapping("/delete")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")/*要想调用此方法必须有ROLE_ADMIN角色*/
    public String delete(){
        System.out.println("add.");
        return "success";
    }
  
}
