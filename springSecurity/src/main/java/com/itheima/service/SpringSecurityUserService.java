package com.itheima.service;

import com.itheima.pojo.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SpringSecurityUserService implements UserDetailsService {
    
    /**
     * 根据用户名查询用户信息
     * <p>
     * 框架拦截之后处理请求，配置了该类，就会反射调用这个方法
     * 页面传来的username,就是这个参数
     */
    //模拟数据库中的用户数据
    public static Map<String, User> map = new HashMap<>();
    
    static {
        com.itheima.pojo.User user1 = new com.itheima.pojo.User();
        user1.setUsername("admin");
        user1.setPassword("admin");//明文密码（没有加密）
        
        com.itheima.pojo.User user2 = new com.itheima.pojo.User();
        user2.setUsername("xiaoming");
        user2.setPassword("1234");
        
        map.put(user1.getUsername(), user1);
        map.put(user2.getUsername(), user2);
    }
    
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        /**根据用户名查询数据库用户信息（包含密码）*/
        User user = map.get(username);//模拟根据用户名查询数据库
        if (user == null) {
            //用户名不存在
            return null;
        }else {
            /**将用户信息返回给框架*/
            /**框架进行密码比对（页面提交，和数据库查询比对）*/
            List<GrantedAuthority> list = new ArrayList<>();//权限集合
            if("admin".equals(username)){
                list.add(new SimpleGrantedAuthority("ROLE_ADMIN"));//授予角色
            }
            list.add(new SimpleGrantedAuthority("permission_A"));//授权
            list.add(new SimpleGrantedAuthority("permission_b"));//授权
            org.springframework.security.core.userdetails.User securityUser =
                    new org.springframework.security.core.userdetails.User(
                            username,"{noop}"+user.getPassword(),list
                    );
            return securityUser;
        }
    }
}
